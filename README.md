# File Guard

File Guard is a program that will make backups of files that is triggered when the file is written to disk.

This small program could be useful in a variety of scenarios, including making backups of video game saves (Dark Souls) or making sure you keep backups of important documents each time you save.

This program was mainly meant as a fun test with Dark Souls game series, where hackers could grief you and you could lose dozens of hours of progress, or worse yet, your entire save.

## Usage

1) Drag and drop a file over the FileGuard.exe and follow the onscreen instructions to use the Max Backups Kept feature

2) place the FileGuard.exe in a directory with the file you want to backup, and enter the file name. If the filename has spaces, enter the entire file name surrounded by quotes like this: `"My File Text.txt"`



