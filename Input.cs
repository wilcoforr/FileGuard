﻿using static System.Console;

namespace FileGuard
{
    public static class Input
    {
        public static string GetUserInput(string str, bool newLine = true)
        {
            Log.Info(str, newLine);
            return ReadLine();
        }
    }
}
