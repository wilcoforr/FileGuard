﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.Console;

namespace FileGuard
{
    /// <summary>
    /// -watch for file date change
    /// -make backup of file
    /// -have a max backup amount
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Information about the file
        /// </summary>
        private static FileInfo _fileInfo;

        /// <summary>
        /// The FileSystemWatcher will trigger events based on when the the file
        /// is lastmodified aka LastWrite to disk.
        /// </summary>
        private static FileSystemWatcher _fileWatcher;

        /// <summary>
        /// The number of max backups to keep. Default is 20, user can set it between 5 and 50
        /// </summary>
        private static int _maxBackupNumber = 20;

        /// <summary>
        /// User can enable whether to limit the number of max backups created
        /// </summary>
        private static bool _checkForMaxBackups = false;

        /// <summary>
        /// Because the FileSystemWatcher raises two events when .Changed event is fired - this makes it so the
        /// program will toggle so that only one backup is created instead of two.
        /// </summary>
        private static bool _createFileToggle = true;

        /// <summary>
        /// Program Init. User can either specify a file or drag and drop a file on top
        /// of the FileGuard.exe to start watching ("guarding") the file
        /// </summary>
        /// <param name="args"></param>
        internal static void Main(string[] args)
        {
            Log.Info("File Guard");

            string fileName = args.Length != 1
                ? Input.GetUserInput("Enter the name of the file.") 
                : args[0];

            if (String.IsNullOrEmpty(fileName))
            {
                Log.Error("Error: no file name entered.");
                ExitProgram();
            }


            CheckForMaxBackups();

            _fileInfo = new FileInfo(fileName);

            if (!_fileInfo.Exists)
            {
                Log.Error($"File '{fileName}' does not exist!");
                ExitProgram();
            }

            //instantiate FileSytemWatcher
            //the reason this is not in a using block is to control the 
            //.Dispose() programmatically rather than in a using block, when bad IO happens.
            _fileWatcher = new FileSystemWatcher()
            {
                Path = _fileInfo.DirectoryName,
                Filter = _fileInfo.Name,
                NotifyFilter = NotifyFilters.LastWrite,
                EnableRaisingEvents = true
            };

            //for debugging
            _fileWatcher.Disposed += (s, e) => 
                Log.Info("File System Watcher released (disposed).");

            //Assign event handler
            _fileWatcher.Changed += HandleFileBackups;


            Log.Info("FileGuard started. Press 'Q' to quit.");

            //block until user presses Q to quit
            while (!String.Equals(ReadKey().KeyChar.ToString(), "Q", StringComparison.OrdinalIgnoreCase)) { }

            WriteLine();
            Log.Info($"User quit. The program will stop monitoring: {_fileInfo.Name}");
            ExitProgram();
        }


        /// <summary>
        /// Dispose of the FileSystemWatcher (if it exists) and exit the program
        /// </summary>
        /// <param name="ex"></param>
        private static void ExitProgram(Exception ex = null)
        {
            _fileWatcher?.Dispose();

            if (ex != null)
            {
                Log.Error(ex);
            }

            Input.GetUserInput("Press enter to exit.");
            Environment.Exit(0);
        }


        /// <summary>
        /// Prompt the user to use the Max Backup Feature.
        /// </summary>
        private static void CheckForMaxBackups()
        {
            string str = "Would you like to set a max backup size? Enter Y to continue to the next prompt for a max backup file count";
            if (Input.GetUserInput(str).ToUpper().Trim() == "Y")
            {
                _checkForMaxBackups = true;

                Log.Info("Will now limit the number of backups made.");
                Log.Info("Enter a whole number (integer) within the range of 5 and 50 (inclusive)");
                if (!Int32.TryParse(Input.GetUserInput("Enter a number: "), out int _maxBackupNumber))
                {
                    Log.Error("A valid value is within the range of 5 to 50 whole numbers (inclusive)");
                    ExitProgram();
                }

                if (_maxBackupNumber < 5 || _maxBackupNumber > 50)
                {
                    Log.Error("A valid value is within the range of 5 to 50 (inclusive)");
                    ExitProgram();
                }

                Log.Info($"FileGuard will now maintain a limit of {_maxBackupNumber} recent file backups.");
            }
        }

        /// <summary>
        /// Handle the creation of a backup
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private static void HandleFileBackups(object s, FileSystemEventArgs e)
        {
            try
            {
                _createFileToggle = !_createFileToggle;

                if (_createFileToggle)
                {
                    string dateTimeStr = "." + DateTime.Now.ToString("MM-dd-yyyy_HH-mm-ss") + ".bak";
                    Log.Info("File changed: " + e.FullPath);
                    File.Copy(_fileInfo.FullName, _fileInfo.FullName + dateTimeStr);

                    if(_checkForMaxBackups)
                    {
                        //after file has been created, delete the oldest one
                        LimitMaxBackups();
                    }
                }
            }
            catch (Exception ex)
            {
                ExitProgram(ex);
            }
        }


        /// <summary>
        /// Limit the number of files backed up. The oldest files are deleted.
        /// This is primitive and will delete the oldest one. Perhaps test to make
        /// sure you cant overflow the backups past the _maxBackupNumber
        /// </summary>
        private static void LimitMaxBackups()
        {
            try
            {
                List<string> files = Directory.GetFiles(_fileInfo.DirectoryName)
                    .Where(f => f.Contains(".bak"))
                    .ToList();

                if (files.Count < _maxBackupNumber)
                {
                    return;
                }

                List<FileInfo> fileInfos = files
                    .Select(f => new FileInfo(_fileInfo.DirectoryName + f))
                    .OrderByDescending(f => f.LastWriteTime)
                    .ToList();

                fileInfos.Last().Delete();
            }
            catch (Exception ex)
            {
                ExitProgram(ex);
            }
        }





    }
}
