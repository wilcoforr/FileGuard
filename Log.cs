﻿using System;

using static System.Console;

namespace FileGuard
{
    internal static class Log
    {
        internal static void LineSeparator() => Info(new string('-', 60));

        internal static void Info(string str, bool newLine = true)
        {
            str = DateTime.Now.ToShortTimeString() + ": " + str;
            if (newLine)
            {
                WriteLine(str);
            }
            else
            {
                Write(str);
            }
        }


        internal static void Error(string str, bool newLine = true)
        {
            ForegroundColor = ConsoleColor.Red;
            Info(str, newLine);
            ForegroundColor = ConsoleColor.Gray;
        }


        internal static void Error(Exception ex)
        {
            Error(ex.Message);
            LineSeparator();
            Error(ex.StackTrace);
        }

    }
}
